const express = require('express');
require('dotenv').config();
const {erroHandler} = require('./midleware/errorMidlware');
const connectDB = require('./db');
const port = process.env.PORT || 8080;

connectDB();

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/api/notes', require('./routes/notesRoutes'));
app.use('/api', require('./routes/userRoutes'));

app.use(erroHandler);

app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});


