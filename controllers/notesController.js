const asyncHandler = require('express-async-handler');

const Note = require('../models/noteModel');
const User = require('../models/userModel');

const fullUrl = (req) => {
  return req.protocol + '://' + req.get('host') + req.originalUrl;
};


const getNotes = asyncHandler(async (req, res) => {
  const offset = parseInt(req.query.offset);
  const limit = parseInt(req.query.limit);

  const notes = await Note.find({userId: req.user.id})
      .limit(limit).skip(offset);


  res.status(200).json({
    offset: offset ? offset : 0,
    limit: limit ? limit : 0,
    count: notes.length,
    notes: notes,
  });
  console.log(200, `${req.method} ${fullUrl(req)}
Success get notes
Notes: ${notes}`);
});


const addNote = asyncHandler(async (req, res) => {
  if (!req.body.text) {
    res.status(400);
    throw new Error('Please add a text field');
  }

  await Note.create({
    userId: req.user.id,
    completed: false,
    text: req.body.text,
    createdDate: Date.now(),
  });

  res.status(200).json({message: 'Success'});
  console.log(200, `${req.method} ${fullUrl(req)}
Success add
User id: ${req.user.id}
Note: ${req.body.text}`);
});


const get1Note = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  res.status(200).json({note: note});
  console.log(200, `${req.method} ${fullUrl(req)}
Success get note
Note: ${note}`);
});


const updateNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }
  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Note.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });

  res.status(200).json({message: 'Success'});
  console.log(200, `${req.method} ${fullUrl(req)}
Success update
Note id: ${req.params.id}
New text: ${req.body.text}`);
});


const deleteNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }
  if (note.userId.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await note.remove();
  res.status(200).json({message: 'Success'});
  console.log(200, `${req.method} ${fullUrl(req)}
Success delete
Note id: ${req.params.id}`);
});


const checkUnchekNote = asyncHandler(async (req, res) => {
  const note = await Note.findById(req.params.id);

  if (!note) {
    res.status(400);
    throw new Error('Note not found');
  }

  await Note.findByIdAndUpdate(req.params.id, {completed: !note.completed});

  res.status(200).json({message: 'Success'});
  console.log(200, `${req.method} ${fullUrl(req)}
Success check/unchek
Note id: ${req.params.id}`);
});

module.exports = {
  getNotes,
  addNote,
  get1Note,
  updateNote,
  deleteNote,
  checkUnchekNote,
};
