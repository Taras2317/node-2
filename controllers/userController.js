const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const fullUrl = (req) => {
  return req.protocol + '://' + req.get('host') + req.originalUrl;
};


const registerUser = asyncHandler(async (req, res) => {
  const {username, password} = req.body;

  if (!username || !password) {
    res.status(400);
    throw new Error('Please fill all fields');
  }

  const userExists = await User.findOne({username});
  if (userExists) {
    res.status(400);
    throw new Error('User already exists');
  }

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  const user = await User.create({
    username,
    password: hashedPassword,
    createdDate: Date.now(),
  });

  if (user) {
    res.status(200).json({message: 'Success'});
    console.log(200, `${req.method} ${fullUrl(req)}
Success registration`);
  } else {
    res.status(400);
    throw new Error('Invalid user data');
  }
});


const loginUser = asyncHandler(async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (user && (await bcrypt.compare(password, user.password))) {
    res.status(200).json({
      message: 'Success',
      jwt_token: generateToken(user._id),
    });
    console.log(200, `${req.method} ${fullUrl(req)}
Success login
JWT_token: ${generateToken(user._id)}`,
    );
  } else {
    res.status(400);
    throw new Error('Invalid credentials');
  }
});


const getUser = asyncHandler(async (req, res) => {
  const {_id, username, createdDate} = await User.findById(req.user.id);

  res.status(200).json({
    user: {
      _id: _id,
      username,
      createdDate,
    },
  });
  console.log(200, `${req.method} ${fullUrl(req)}
Success user get
User id: ${_id}
User name: ${username}
Created date: ${createdDate}`);
});


const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user?.id);

  if (!user) {
    res.status(400);
    throw new Error('User dont exists');
  }

  await user.remove();

  res.status(200).json({message: 'Success'});
  console.log(200, `${req.method} ${fullUrl(req)}
Success user delete`);
});

const changePassword = asyncHandler(async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user?.id);

  if (!oldPassword || !newPassword) {
    res.status(400);
    throw new Error('Please fill all fields');
  }

  const salt = await bcrypt.genSalt(10);
  const newHashedPassword = await bcrypt.hash(newPassword, salt);

  if (await bcrypt.compare(oldPassword, user.password)) {
    await User.findByIdAndUpdate(req.user?.id, {password: newHashedPassword});
    res.status(200).json({message: 'Success'});
    console.log(200, `${req.method} ${fullUrl(req)}
Successfully updated password`);
  } else {
    res.status(400);
    throw new Error('Wrong old password');
  }
});

// Generate JWT
const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '30d',
  });
};

module.exports = {
  registerUser,
  loginUser,
  getUser,
  deleteUser,
  changePassword,
};
