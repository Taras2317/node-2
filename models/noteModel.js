const mongoose = require('mongoose');

/* eslint new-cap: ["error", { "capIsNewExceptions": ["Schema"] }] */
const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  completed: Boolean,
  text: {
    type: String,
    required: [true, 'Please add a text value'],
  },
  createdDate: Date,
}, {versionKey: false});

module.exports = mongoose.model('Note', noteSchema);
