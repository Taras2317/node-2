const mongoose = require('mongoose');

/* eslint new-cap: ["error", { "capIsNewExceptions": ["Schema"] }] */
const userSchema = mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Please add a name'],
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
  },
  createdDate: Date,
}, {versionKey: false});

module.exports = mongoose.model('User', userSchema);
