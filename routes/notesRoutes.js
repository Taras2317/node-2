const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }] */
const router = express.Router();
const {
  getNotes,
  addNote,
  get1Note,
  updateNote,
  deleteNote,
  checkUnchekNote,
} = require('../controllers/notesController');

const {protect} = require('../midleware/authMidleware');

router.route('/').get(protect, getNotes).post(protect, addNote);
router.route('/:id')
    .get(protect, get1Note)
    .put(protect, updateNote)
    .delete(protect, deleteNote)
    .patch(protect, checkUnchekNote);

module.exports = router;
