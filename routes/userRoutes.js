const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }] */
const router = express.Router();

const {
  registerUser,
  loginUser,
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');
const {protect} = require('../midleware/authMidleware');

router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.get('/users/me', protect, getUser);
router.delete('/users/me', protect, deleteUser);
router.patch('/users/me', protect, changePassword);

module.exports = router;
